#!/bin/bash

set -e
set -x

git submodule update --recursive

if [ -d tvh ]
then
    rm -rf tvh
fi

mkdir tvh

cp picons-australia/picon/*.png tvh/

if [ -z "$(git status --porcelain)" ]; then
    echo "No changes to be made"
else
    git add tvh
    git add -u tvh
    git add picons-australia

    git commit -m "Update picons-australia picons"
fi
